/**
 * Created by Dawid Stankiewicz on 18.07.2016
 */
package com.project.admodb.entity.repository;

import com.project.admodb.entity.Topic;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.admodb.entity.Post;
import com.project.admodb.entity.User;


public interface PostRepository extends JpaRepository<Post, Integer> {
    
    Set<Post> findByUser(User user);
    
    Set<Post> findByTopic(Topic topic);
    
    Set<Post> findAllByOrderByCreationDateDesc();
    
    Set<Post> findTop5ByOrderByCreationDateDesc();

    Post findById(int id);
}
