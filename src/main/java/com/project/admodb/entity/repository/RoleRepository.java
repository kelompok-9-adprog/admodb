/**
 * Created by Dawid Stankiewicz on 22.07.2016
 */
package com.project.admodb.entity.repository;

import com.project.admodb.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Integer> {
    
    Role findByName(String name);
    Role findById(int id);
    
}
