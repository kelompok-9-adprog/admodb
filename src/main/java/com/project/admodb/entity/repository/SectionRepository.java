/**
 * Created by Dawid Stankiewicz on 17.07.2016
 */
package com.project.admodb.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.admodb.entity.Section;


public interface SectionRepository extends JpaRepository<Section, Integer> {
    
    Section findByName(String name);
    Section findById(int id);
    
}
