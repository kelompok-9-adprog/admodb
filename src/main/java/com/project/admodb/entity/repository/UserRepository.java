/**
 * Created by Dawid Stankiewicz on 3 Jul 2016
 */
package com.project.admodb.entity.repository;

import com.project.admodb.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {
    
    User findByUsername(String username);
    
    User findByEmail(String email);

    User findById(int id);
    
}
