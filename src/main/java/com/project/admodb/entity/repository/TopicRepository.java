/**
 * Created by Dawid Stankiewicz on 18.07.2016
 */
package com.project.admodb.entity.repository;

import com.project.admodb.entity.Section;
import com.project.admodb.entity.Topic;
import com.project.admodb.entity.User;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;


public interface TopicRepository extends JpaRepository<Topic, Integer> {
    
    Set<Topic> findBySection(Section section);
    
    Set<Topic> findByUser(User user);
    
    Set<Topic> findAllByOrderByCreationDateDesc();
    
    Set<Topic> findTop5ByOrderByCreationDateDesc();

    Topic findById(int id);
    
    
}
