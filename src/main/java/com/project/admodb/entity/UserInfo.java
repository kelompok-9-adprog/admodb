package com.project.admodb.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "users_info", schema = "ap_db")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 398881966654252337L;

    @Id
    @Column(name = "user_id")
    private int id;

    @MapsId
    @OneToOne
    @JoinColumn(name = "users", referencedColumnName = "id")
    private User user;


    @Column(length = 150)
    private String aboutMe;

    @Column(length = 50)
    private String footer;

    @Column
    private Date joinedDate;

    @Column
    private boolean removed;

//    @Column
//    private int posts;
//
//    @Column
//    private int topics;

    public UserInfo() {
    }

    @PrePersist
    public void onCreate() {
        this.joinedDate = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public Date getJoinedDate() {
        return joinedDate;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

//    public int getPosts() {
//        return posts;
//    }
//
//    public void setPosts(int posts) {
//        this.posts = posts;
//    }
//
//    public int getTopics() {
//        return topics;
//    }
//
//    public void setTopics(int topics) {
//        this.topics = topics;
//    }
}
