package com.project.admodb.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "users", schema = "ap_db")
@ApiModel(description = "All details about the User.")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "increment")
    private int id;

    @Column(length = 255, nullable = false, unique = true)
    private String email;

    @Column(length = 30, nullable = false, unique = true)
    private String username;

    @Column(length = 50)
    private String name;

    @Column(length = 60, nullable = false)
    private String password;

    @Column
    private boolean active;

    @Column(length = 60)
    private String footer;

    @Column(columnDefinition = "TEXT")
    private String idProfilePicture;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "rolesOfUser",
        joinColumns = @JoinColumn(name = "users", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "role", referencedColumnName = "id"))
    private Set<Role> roles;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private UserInfo info;

    public User() {
    }

    public User(String name, String email, String username, String password){

        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName(){ return name; }

    public void setName(String name) { this.name = name; }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public UserInfo getInfo() {
        return info;
    }

    public void setInfo(UserInfo info) {
        this.info = info;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getIdProfilePicture() {
        return idProfilePicture;
    }

    public void setIdProfilePicture(String idProfilePicture) {
        this.idProfilePicture = idProfilePicture;
    }
}
