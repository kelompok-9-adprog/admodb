/**
 * Created by Dawid Stankiewicz on 04.08.2016
 */
package com.project.admodb.service.model;

import com.project.admodb.controller.model.UserProfile;


public interface UserProfileService {
    
    public UserProfile findOne(int userId);
    
    public UserProfile findOne(String username);
    
}
