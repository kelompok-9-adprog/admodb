/**
 * Created by Dawid Stankiewicz on 04.08.2016
 */
package com.project.admodb.service.model.impl;

import com.project.admodb.controller.model.UserProfile;
import com.project.admodb.entity.User;
import com.project.admodb.service.PostService;
import com.project.admodb.service.TopicService;
import com.project.admodb.service.model.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.admodb.service.UserService;


@Service
public class UserProfileServiceImpl implements UserProfileService {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private PostService postService;
    
    @Autowired
    private TopicService topicService;
    
    @Override
    public UserProfile findOne(int userId) {
        UserProfile userProfile = new UserProfile();
        User user = userService.findOne(userId);
        userProfile.setUser(user);
        userProfile.setPosts(postService.findByUser(user));
        userProfile.setTopics(topicService.findByUser(user));
        return userProfile;
    }
    
    @Override
    public UserProfile findOne(String username) {
        return findOne(userService.findByUsername(username).getId());
    }
    
}
