package com.project.admodb.controller;

import com.project.admodb.service.PostService;
import com.project.admodb.service.SectionService;
import com.project.admodb.service.TopicService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = HomeController.class)
public class HomeControllerTest{

    @Autowired
    private MockMvc mockHomeController;

    @MockBean
    private SectionService sectionService;

    @MockBean
    private TopicService topicService;

    @MockBean
    private PostService postService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void testHomePage() throws Exception {
        mockHomeController.perform(get("/home")).andExpect(view().name("home")).andExpect(model().attributeExists(
                "sections")).andExpect(model().attributeExists("topics")).andExpect(model()
                        .attributeExists("posts"));
    }

}
