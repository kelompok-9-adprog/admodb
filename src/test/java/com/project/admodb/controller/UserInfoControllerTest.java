package com.project.admodb.controller;

import com.project.admodb.entity.User;
import com.project.admodb.entity.UserInfo;
import com.project.admodb.service.UserService;
import com.project.admodb.service.model.UserProfileService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
public class UserInfoControllerTest {

    @Autowired
    private MockMvc mockHomeController;

    @MockBean
    private UserService userService;

    @MockBean
    private UserProfileService userProfileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void testCreateUserWithInfo() throws Exception {

        String name = "John Mardun";
        String email = "john@gmail.com";
        String username = "john99";
        String password = "john123";
        String footer = "";
        Boolean active = true;
        String idProfilePicture = "test.jpg";

        User dummy = new User();
        dummy.setId(1);
        dummy.setName(name);
        dummy.setEmail(email);
        dummy.setUsername(username);
        dummy.setPassword(password);
        dummy.setActive(true);
        dummy.setFooter("");
        dummy.setIdProfilePicture(idProfilePicture);

        Assert.assertEquals(1,dummy.getId());
        Assert.assertEquals(name,dummy.getName());
        Assert.assertEquals(email, dummy.getEmail());
        Assert.assertEquals(username, dummy.getUsername());
        Assert.assertEquals(password, dummy.getPassword());
        Assert.assertEquals(active, dummy.isActive());
        Assert.assertEquals(footer, dummy.getFooter());
        Assert.assertEquals(idProfilePicture,dummy.getIdProfilePicture());

        // FOR DUMMY USERINFO
        UserInfo dummyInfo = new UserInfo();
        dummyInfo.setId(1);
        dummyInfo.setAboutMe("");
        dummyInfo.setRemoved(false);
        dummyInfo.setFooter("");
        dummyInfo.setUser(dummy);
        dummyInfo.onCreate();

        // SET USER INFO from DUMMY (user)
        dummy.setInfo(dummyInfo);

        Assert.assertEquals(1,dummyInfo.getId());
        Assert.assertEquals("", dummyInfo.getAboutMe());
        Assert.assertEquals(dummy, dummyInfo.getUser());
        Assert.assertEquals(false,dummyInfo.isRemoved());
        Assert.assertEquals(footer, dummyInfo.getFooter());
        Assert.assertEquals(dummyInfo, dummy.getInfo());


        User dummy2 = new User(name,email,username,password);

        mockHomeController.perform(post("/registration")
                .param("username", dummy.getName())
                .param("password", dummy.getPassword())
                .param("username", dummy.getUsername())
                .param("name", dummy.getName()))
                .andExpect(status().isOk());

        mockHomeController.perform(post("/registration")
                .param("username", dummy2.getName())
                .param("password", dummy2.getPassword())
                .param("username", dummy2.getUsername())
                .param("name", dummy2.getName()))
                .andExpect(status().isOk());

    }
}
